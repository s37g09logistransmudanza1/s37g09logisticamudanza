-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-10-2021 a las 05:30:51
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `moving`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso`
--

CREATE TABLE `acceso` (
  `usuario` varchar(20) NOT NULL,
  `contraseña` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acceso`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `tipo_id` varchar(2) NOT NULL,
  `num_id` int(11) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `telefono` int(11) UNSIGNED NOT NULL,
  `mail` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `tipo_id`, `num_id`, `direccion`, `telefono`, `mail`) VALUES
(7, 'Aseneth', 'CC', 345678, 'cra 2#3-4', 3138877903, 'aseneth@gmail.com'),
(8, 'Jackson', 'CC', 67890, 'CALLE 17 N 17 25', 3165754775, 'zbogota@gmail.com'),
(9, 'Leidy Jaimes', 'CC', 195278, 'calle 168#2-4', 3002519930, 'leidyjaimestorres@gmail.com'),
(10, 'Juan Buitrago', 'CC', 567890, 'calle 1#2-3', 3192992356, 'jcbuitragov@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id_orden_servicio` int(11) NOT NULL,
  `item` tinyint(3) UNSIGNED NOT NULL,
  `cantidad` tinyint(4) NOT NULL,
  `elemento` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_de_servicio`
--

CREATE TABLE `orden_de_servicio` (
  `id_orden_servicio` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `tipo_servicio` varchar(11) NOT NULL,
  `volumen` smallint(6) NOT NULL,
  `embalaje` tinyint(1) NOT NULL,
  `bodegaje` tinyint(1) NOT NULL,
  `dirección_origen` varchar(20) NOT NULL,
  `direccion_destino` varchar(20) NOT NULL,
  `valor` int(11) NOT NULL,
  `tipo_vehiculo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `id_vehiculo` tinyint(4) NOT NULL,
  `tipo_vehiculo` varchar(12) NOT NULL,
  `capacidad` tinyint(4) NOT NULL,
  `valor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`id_vehiculo`, `tipo_vehiculo`, `capacidad`, `valor`) VALUES
(1, 'NHR', 12, 400000),
(2, 'TURBO', 25, 800000),
(3, 'SENCILLO', 35, 1600000),
(4, 'TRACTOCAMION', 70, 2200000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD UNIQUE KEY `id_orden_servicio` (`id_orden_servicio`,`item`);

--
-- Indices de la tabla `orden_de_servicio`
--
ALTER TABLE `orden_de_servicio`
  ADD PRIMARY KEY (`id_orden_servicio`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `tipo_vehiculo` (`tipo_vehiculo`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`id_vehiculo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `orden_de_servicio`
--
ALTER TABLE `orden_de_servicio`
  MODIFY `id_orden_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`id_orden_servicio`) REFERENCES `orden_de_servicio` (`id_orden_servicio`);

--
-- Filtros para la tabla `orden_de_servicio`
--
ALTER TABLE `orden_de_servicio`
  ADD CONSTRAINT `orden_de_servicio_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `orden_de_servicio_ibfk_2` FOREIGN KEY (`tipo_vehiculo`) REFERENCES `vehiculo` (`id_vehiculo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
