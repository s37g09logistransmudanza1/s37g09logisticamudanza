#importar modulo de flask
from flask import Flask,render_template, url_for,redirect,request
#importar modulo de MySQL
#from flaskext.mysql import MySQL
import controladordb

# variable para establecer la app desde flask
app=Flask(__name__)
#Variables GLOBALES
nombrecliente=''
idcliente=0
#Rutas principal
@app.route('/')
#Asocio a un metodo
def acceso():
    return render_template('acceso.html')

@app.route('/menu')
def menu(rstDB= None):
    if rstDB == None:
        rstDB = controladordb.read_dat()
    return render_template('menu.html', rstDB=rstDB)

@app.route('/guardar_datos',methods =['GET','POST'])
def guardar_datos():
    if request.method == 'POST':
        # guardar las variables de usuario
        usuario =request.form['usuario']
        pasword =request.form['pasword']
        rstDB = controladordb.insert_dat(usuario, pasword)
        #return f"<h1>{rstDB}</h1>"
        return redirect(url_for('menu'))

@app.route('/registro')
def registro():
    return render_template('registro.html')

# Guarda los datos del cliente
@app.route('/registro_datos',methods =['GET','POST'])
def registro_datos():
    if request.method == 'POST':
        nombre =request.form['nombre']
        global nombrecliente
        nombrecliente = nombre
        tipoID =request.form['tipoID']
        numeroid =request.form['numeroid']
        direccion =request.form['direccion']
        telefono =request.form['telefono']
        mail =request.form['email']
        rstDB = controladordb.insert_registro(nombre,tipoID,numeroid,direccion,telefono,mail)
        return render_template('servicio.html')
# Guarda los datos del servicio
@app.route('/servicio_datos',methods =['GET','POST'])
def servicio_datos():
    print(nombrecliente)
    global idcliente
    idcliente = controladordb.read_id_cliente(nombrecliente)
    if request.method == 'POST':
        tipo_servicio=request.form['tiposervicio']
        volumen =request.form['volumen']
        embalaje =request.form['embalaje']
        bodegaje =request.form['bodegaje']
        dirección_origen =request.form['origen']
        direccion_destino =request.form['destino']
        rstDB = controladordb.insert_cliente(idcliente,tipo_servicio,volumen,embalaje,bodegaje,dirección_origen,direccion_destino)
        return redirect(url_for('ticket'))

# Elimina los datos del cliente por el id
@app.route('/eliminar_datos',methods =['GET','POST'])
def eliminar_datos():
    if request.method == 'POST':
        id_c =request.form['id']
        rstDB = controladordb.delete_dat_orden(id_c)
        return redirect(url_for('consulta'))
# Edita los datos del cliente por el id
@app.route('/editar_datos',methods =['GET','POST'])
def editar_datos():
    if request.method == 'POST':
        id =request.form['id']
        rstDB = controladordb.read_id(id)
        return render_template('editar_registro.html',rstDB=rstDB)

# Edita los datos del orden por el id
@app.route('/editar_servicio',methods =['GET','POST'])
def editar_servicio():
    print("XXXXXX")
    if request.method == 'POST':
        id =request.form['id']
        print(id)
        rstDB = controladordb.read_id_orden(id)
        return render_template('editar_servicio.html',rstDB=rstDB)

# Actualiza los datos del cliente
@app.route('/actualizar_datos',methods =['GET','POST'])
def actualizar_datos():
    if request.method == 'POST':
        id_c =request.form['id']
        nombre =request.form['nombre']
        tipoID =request.form['tipoID']
        numeroid =request.form['numeroid']
        direccion =request.form['direccion']
        telefono =request.form['telefono']
        mail =request.form['email']
        rstDB = controladordb.update_dat(id_c,nombre,tipoID,numeroid,direccion,telefono,mail)
        return redirect(url_for('consulta'))  
# Actualiza los datos del servicio
@app.route('/actualizar_servicio',methods =['GET','POST'])
def actualizar_servicio():
    if request.method == 'POST':
        id_o =request.form['id_orden']
        tipo_servicio=request.form['tiposervicio']
        volumen =request.form['volumen']
        embalaje =request.form['embalaje']
        bodegaje =request.form['bodegaje']
        origen =request.form['origen']
        destino =request.form['destino']
        rstDB = controladordb.update_dat_orden(id_o,tipo_servicio,volumen,embalaje,bodegaje,origen,destino)
        return redirect(url_for('consulta'))   
@app.route('/servicio')
def servicio():
    return render_template('servicio.html')
@app.route('/inventario')
def inventario():
    return render_template('inventario.html')
@app.route('/ticket')
def ticket(rstDB= None):
    print(idcliente)
    if rstDB == None:
        rstDB = controladordb.read_id_orden(idcliente)
    return render_template('ticket.html', rstDB=rstDB)

@app.route('/consulta')
def consulta(rstDB= None):
    if rstDB == None:
        rstDB = controladordb.read_dat_orden()
    return render_template('consulta.html', rstDB=rstDB)


# Establecer el fichero principal
if __name__=='__main__':
    #(debug=True) genera arranque el servidor para flask
    app.run(debug=True)