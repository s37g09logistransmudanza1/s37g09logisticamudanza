from conexiondb import conexion_db

#create
def insert_dat(usuario, pasword):
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #insertar  datos base en la tabla
        sql = f"INSERT INTO acceso(Usuario, Contraseña) VALUES('{usuario}','{pasword}')"
        cursor.execute(sql)
        # guardando los datos
        conexion.commit()
    conexion.close()
    return "Datos guardados"

def insert_registro(nombre,tipoID,numeroid,direccion,telefono,mail):
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        sql = f"INSERT INTO cliente(id_cliente, nombre, tipo_id, num_id, direccion, telefono, mail) VALUES(Null,'{nombre}','{tipoID}',{numeroid},'{direccion}',{telefono},'{mail}')"
        cursor.execute(sql)
        # guardando los datos
        conexion.commit()
    conexion.close()
    return "Datos guardados registro"

def insert_cliente(idcliente,tipo_servicio,volumen,embalaje,bodegaje,direccion_origen,direccion_destino):
    print(idcliente)
    conexion = conexion_db()
    rstDB =[]
    apuntador=conexion.cursor()
    sql='SELECT * FROM `vehiculo` ORDER BY capacidad'
    apuntador.execute(sql)
    capacidad=[0]
    valor=[0]
    for v in apuntador:
        capacidad.append(v[2])
        valor.append(v[3])
    tipo_veh=[]
    volumen=int(volumen)
    for i in range(0,len(capacidad)-1):    
        if volumen>capacidad[i] and volumen<=capacidad[i+1]:
            id_tipo_veh=i+1
    valor=0
    sql=f'SELECT valor,tipo_vehiculo FROM vehiculo WHERE id_vehiculo={id_tipo_veh}'
    apuntador.execute(sql)
    v= apuntador.fetchone()
    valor=(v[0])
    tipo_veh=(v[1])   

    with conexion.cursor() as cursor:
        sql = f"INSERT INTO orden_de_servicio(id_orden_servicio, id_cliente, tipo_servicio, volumen, embalaje, bodegaje, origen, direccion_destino,valor,tipo_vehiculo) VALUES(Null,{idcliente},'{tipo_servicio}',{volumen},'{embalaje}','{bodegaje}','{direccion_origen}','{direccion_destino}',{valor},'{tipo_veh}')"
        #sql = "INSERT INTO orden_de_servicio(id_orden_servicio, id_cliente, tipo_servicio, volumen, embalaje, bodegaje, origen, direccion_destino,valor,tipo_vehiculo) VALUES(Null,14,'a',22,1,0,'as','cas3',1,1)"
        cursor.execute(sql)
        # guardando los datos
        conexion.commit()
    conexion.close()
    return "Datos guardados cliente"

#read all cliente
def read_dat():
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #consulta de datos base en la tabla
        sql ="SELECT *FROM cliente"
        cursor.execute(sql)
        rstDB = cursor.fetchall()
    conexion.close()
    return rstDB
#read id CLIENTE 
def read_id_cliente(nombrecliente):
    print('XXXXXXXXXXX'+nombrecliente)
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        #consulta de datos base en la tabla
        sql =f"SELECT id_cliente FROM cliente WHERE  nombre= '{nombrecliente}'"
        cursor.execute(sql)
        rstDB = cursor.fetchone()
    conexion.close()
    return rstDB[0]


#read id
def read_id(id):
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #consulta de datos base en la tabla
        sql =f"SELECT *FROM cliente WHERE id_cliente = {id}"
        cursor.execute(sql)
        rstDB = cursor.fetchone()
    conexion.close()
    return rstDB
#read all orden
def read_dat_orden():
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #consulta de datos base en la tabla
        sql ="SELECT *FROM orden_de_servicio"
        cursor.execute(sql)
        rstDB = cursor.fetchall()
    conexion.close()
    return rstDB
def read_id_orden(id):
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #consulta de datos base en la tabla
        sql =f"SELECT *FROM orden_de_servicio WHERE id_cliente = {id}"
        cursor.execute(sql)
        rstDB = cursor.fetchone()
    conexion.close()
    return rstDB
#update
def upgrade_dat():
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #insertar  datos base en la tabla
        sql =" UPDATE acceso SET Usuario='Juan', Contraseña='54322'"
        cursor.execute(sql)
        # guardando los datos
        conexion.commit()
    conexion.close()
    return "Datos actualizados"
#update Orde
def update_dat_orden(id_o,tipo_servicio,volumen,embalaje,bodegaje,origen,destino):
    conexion = conexion_db()
    rstDB =[]
    with conexion.cursor() as cursor:
        #insertar  datos base en la tabla
        #sql ="UPDATE orden_de_servicio SET tipo_servicio='Expreso', volumen=2, embalaje='Si', bodegaje='No', origen='calle 1', direccion_destino='calle 20',valor=4,tipo_vehiculo=1 WHERE id_orden_servicio=23 "
        sql =f"UPDATE orden_de_servicio SET tipo_servicio='{tipo_servicio}', embalaje='{embalaje}', bodegaje='{bodegaje}', origen='{origen}', direccion_destino='{destino}' WHERE id_orden_servicio={id_o} "
        cursor.execute(sql)
        # guardando los datos
        conexion.commit()
    conexion.close()
    return "Datos actualizados"

#Delete
def delete_dat(id_c):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        #elimina los datos de cliente dependiendo el id
        sql =f" DELETE FROM cliente WHERE id_cliente = {id_c}"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos eliminado"
#Delete
def delete_dat_orden(id_c):
    conexion = conexion_db()
    with conexion.cursor() as cursor:
        #elimina los datos de cliente dependiendo el id de la orden
        sql =f" DELETE FROM orden_de_servicio WHERE id_orden_servicio = {id_c}"
        cursor.execute(sql)
        conexion.commit()
    conexion.close()
    return "Datos eliminado"